"""Unit tests for common components."""

import unittest

import check_zpool


class CommonTest(unittest.TestCase):
    """Unit tests for common components."""

    def test_emit_status(self):
        """Test if the emit function returns an appropriate exit code."""
        for state in check_zpool.STATES:
            with self.assertRaises(SystemExit) as context:
                check_zpool.emit('TESTCHECK', state, 'testmsg')
            self.assertEqual(context.exception.code, check_zpool.STATES[state])

    def test_human_readable_bytes(self):
        """Test if the human_readable_bytes function works as expected."""
        self.assertEqual(check_zpool.human_readable_bytes(0), '0 B')
        self.assertEqual(check_zpool.human_readable_bytes(1), '1 B')
        self.assertEqual(check_zpool.human_readable_bytes(1023), '1023 B')
        self.assertEqual(check_zpool.human_readable_bytes(1024), '1 KiB')
        self.assertEqual(check_zpool.human_readable_bytes(1536), '1.5 KiB')
        self.assertEqual(
            check_zpool.human_readable_bytes(137438953472), '128 GiB'
        )
        self.assertEqual(
            check_zpool.human_readable_bytes(3693645496320), '3.36 TiB'
        )
        self.assertEqual(check_zpool.human_readable_bytes(1024**5), '1 PiB')
        self.assertEqual(
            check_zpool.human_readable_bytes(1024**6 + 512 * 1024**5),
            '1536 PiB'
        )
