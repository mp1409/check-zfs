"""Script to create test output, mimicing the zpool command."""

import sys

PROP = None
POOLNAME = None

for p in ('health', 'capacity', 'free'):
    if p in sys.argv:
        PROP = p
        POOLNAME = sys.argv[sys.argv.index(p) + 1]

if PROP == 'health':
    if POOLNAME == 'onlinepool':
        print('ONLINE')
    elif POOLNAME == 'degradedpool':
        print('DEGRADED')
    elif POOLNAME == 'faultedpool':
        print('FAULTED')
    elif POOLNAME == 'emptyoutput':
        pass
    elif POOLNAME == 'badcommand':
        print('garbage')
        sys.exit(1)
    else:
        raise RuntimeError('Invalid call to zpool.py')

elif PROP == 'capacity':
    if POOLNAME == 'normalpool1':
        print(3)
    elif POOLNAME == 'normalpool2':
        print(68)
    elif POOLNAME == 'warningpool':
        print(85)
    elif POOLNAME == 'criticalpool':
        print(100)
    elif POOLNAME == 'emptyoutput':
        pass
    elif POOLNAME == 'badoutput1':
        print('bad')
    elif POOLNAME == 'badoutput2':
        print(200)
    elif POOLNAME == 'badcommand':
        print('garbage')
        sys.exit(1)
    else:
        raise RuntimeError('Invalid call to zpool.py')

elif PROP == 'free':
    if POOLNAME == 'normalpool1':
        print(137248489984)
    elif POOLNAME == 'normalpool2':
        print(3753708380160)
    elif POOLNAME == 'warningpool':
        print(214748364800)
    elif POOLNAME == 'criticalpool':
        print(0)
    elif POOLNAME == 'emptyoutput':
        pass
    elif POOLNAME == 'badoutput1':
        print('bad')
    elif POOLNAME == 'badoutput2':
        print('-10')
    elif POOLNAME == 'badcommand':
        print('garbage')
        sys.exit(1)
    else:
        raise RuntimeError('Invalid call to zpool.py')

else:
    raise RuntimeError('Invalid call to zpool.py')
