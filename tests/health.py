"""Unit tests concerning the health check."""

import os.path
import sys
import unittest

import check_zpool


class HealthTest(unittest.TestCase):
    """Unit tests concerning the health check."""

    def setUp(self):
        mockup_zpoolcmd_path = os.path.dirname(__file__) + '/zpool.py'
        check_zpool.ZPOOL_COMMAND = sys.executable + ' ' + mockup_zpoolcmd_path

    def test_online_pool(self):
        """Test the health of an online pool."""
        status, message = check_zpool.check_health('onlinepool')
        self.assertEqual(status, 'OK')
        self.assertEqual(message, 'status: ONLINE')

    def test_degraded_pool(self):
        """Test the health of a degraded pool."""
        status, message = check_zpool.check_health('degradedpool')
        self.assertEqual(status, 'CRITICAL')
        self.assertEqual(message, 'status: DEGRADED')

    def test_faulted_pool(self):
        """Test the health of a faulted pool."""
        status, message = check_zpool.check_health('faultedpool')
        self.assertEqual(status, 'CRITICAL')
        self.assertEqual(message, 'status: FAULTED')

    def test_empty_output(self):
        """Test if the health check can handle empty output."""
        status, _ = check_zpool.check_health('emptyoutput')
        self.assertEqual(status, 'CRITICAL')

    def test_bad_command(self):
        """
        Test if the health check can handle a nonfunctional zpool command.
        """
        status, message = check_zpool.check_health('badcommand')
        self.assertEqual(status, 'UNKNOWN')
        self.assertEqual(message, 'zpool command could not be executed!')

    def test_nonexistent_command(self):
        """Test if the health check can handle a nonexisting zpool command."""
        check_zpool.ZPOOL_COMMAND = '/nonexistent'

        status, message = check_zpool.check_health('does not matter')
        self.assertEqual(status, 'UNKNOWN')
        self.assertEqual(message, 'zpool command could not be executed!')
