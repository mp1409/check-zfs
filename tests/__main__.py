"""Main module of the test suite, invoking all other tests."""

import os.path
import sys
import unittest

import coverage

projectpath = os.path.dirname(__file__) + '/..'
sys.path.append(projectpath)
datafilepath = projectpath + '/tests/coverage/data'
reportpath = projectpath + '/tests/coverage'

cov = coverage.Coverage(data_file=datafilepath)
cov.start()

# pylint: disable=wrong-import-position,unused-import
from common import CommonTest
from health import HealthTest
from capacity import CapacityTest

result = unittest.main(exit=False).result

cov.stop()
cov.save()

print('=' * 70 + '\nCOVERAGE\n' + '-' * 70)
cov.report()
cov.html_report(directory=reportpath)

sys.exit(not result.wasSuccessful())
