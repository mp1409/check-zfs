"""Unit tests concerning the capacity check."""

import os.path
import sys
import unittest

import check_zpool


class CapacityTest(unittest.TestCase):
    """Unit tests concerning the capacity check."""

    def setUp(self):
        mockup_zpoolcmd_path = os.path.dirname(__file__) + '/zpool.py'
        check_zpool.ZPOOL_COMMAND = sys.executable + ' ' + mockup_zpoolcmd_path

    def test_normal_pools(self):
        """
        Test the capacity of a normal pool (output captured from live system).
        """
        status, message = check_zpool.check_capacity('normalpool1')
        self.assertEqual(status, 'OK')
        self.assertEqual(
            message,
            'fill level: 3% (127.82 GiB free space) '
            '| fill_level=3%;80;90 free_space=137248489984B'
        )

        status, message = check_zpool.check_capacity('normalpool2')
        self.assertEqual(status, 'OK')
        self.assertEqual(
            message,
            'fill level: 68% (3.41 TiB free space) '
            '| fill_level=68%;80;90 free_space=3753708380160B'
        )

    def test_warning_pool(self):
        """Test the capacity of a relatively full pool."""
        status, message = check_zpool.check_capacity('warningpool')
        self.assertEqual(status, 'WARNING')
        self.assertEqual(
            message,
            'fill level: 85% (200 GiB free space) '
            '| fill_level=85%;80;90 free_space=214748364800B'
        )

    def test_critical_pool(self):
        """Test the capacity of a full pool."""
        status, message = check_zpool.check_capacity('criticalpool')
        self.assertEqual(status, 'CRITICAL')
        self.assertEqual(
            message,
            'fill level: 100% (0 B free space) '
            '| fill_level=100%;80;90 free_space=0B'
        )

    def test_empty_output(self):
        """Test if the capacity check can handle empty output."""
        status, _ = check_zpool.check_capacity('emptyoutput')
        self.assertEqual(status, 'UNKNOWN')

    def test_bad_output(self):
        """Test if the capacity check can handle bad output."""
        status, message = check_zpool.check_capacity('badoutput1')
        self.assertEqual(status, 'UNKNOWN')
        self.assertEqual(message, 'zpool command output could not be parsed!')

        status, message = check_zpool.check_capacity('badoutput2')
        self.assertEqual(status, 'UNKNOWN')
        self.assertEqual(message, 'zpool command output could not be parsed!')

    def test_bad_command(self):
        """
        Test if the capacity check can handle a nonfunctional zpool command.
        """
        status, message = check_zpool.check_capacity('badcommand')
        self.assertEqual(status, 'UNKNOWN')
        self.assertEqual(message, 'zpool command could not be executed!')

    def test_nonexistent_command(self):
        """
        Test if the capacity check can handle a nonexisting zpool command.
        """
        check_zpool.ZPOOL_COMMAND = '/nonexistent'

        status, message = check_zpool.check_capacity('does not matter')
        self.assertEqual(status, 'UNKNOWN')
        self.assertEqual(message, 'zpool command could not be executed!')
