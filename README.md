# check-zfs [![build status](https://gitlab.com/mp1409/check-zfs/badges/master/build.svg)](https://gitlab.com/mp1409/check-zfs/commits/master) #
This project consists of a script to check the capacity and health of ZFS pools.
It was developed to be run by Nagios via NRPE, but can also be used with monit.

## Installation ##
The script only requires a standard python installation.
In production it has sucessfully been used in the following configurations:
* FreeBSD 10.1 with CPython 3.5

The unit test are successful on the following configurations:
* CPython 3.6
* CPython 3.5
* CPython 3.4
* CPython 3.3
* PyPy 3-5.5
* CPython 2.7
* PyPy 2-5.6

To install, simply copy the `check_zpool.py` file to a suitable directory.
For example:
```
sudo cp check_zpool.py /usr/local/libexec/nagios-custom
```

## Usage ##
The script can then be run by:
```
python3 <installpath>/check_zpool.py <health|capacity> <poolname>
```

For example:
```
python3 /usr/local/libexec/nagios-custom/check_zpool.py health zroot
python3 /usr/local/libexec/nagios-custom/check_zpool.py capacity tank
```

It should then be added to `/usr/local/etc/nrpe.cfg`:
```
command[check_zroot_health]=/usr/local/bin/python3 /usr/local/libexec/nagios-custom/check_zpool.py health zroot
command[check_tank_health]=/usr/local/bin/python3 /usr/local/libexec/nagios-custom/check_zpool.py health tank

command[check_zroot_disk]=/usr/local/bin/python3 /usr/local/libexec/nagios-custom/check_zpool.py capacity zroot
command[check_tank_disk]=/usr/local/bin/python3 /usr/local/libexec/nagios-custom/check_zpool.py capacity tank
```

## Development ##
The check_zpool script alone can be run on any platform mentioned above, the same goes for the unit tests, but they need the additional `coverage` package.
The utility scripts mentioned below require at least Python 3.4 and the `pylint` and `pep8` packages.

### Unit tests ###
The unit test suite can be run by invoking the `tests` module:
```
python tests
```
A coverage report is generated and saved to `tests/coverage/index.html`.

### Source code checking ###
The source code can be tested with both [pylint](https://pylint.org/) and [pep8](https://github.com/PyCQA/pycodestyle) by running:
```
utils/lint.py
```

### Cleaning build leftovers ###
Leftovers from previous runs (for example coverage reports) can be removed by:
```
utils/clean.py
```
