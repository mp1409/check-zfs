"""Clean leftovers from previous runs."""

from pathlib import Path
import shutil

scriptdir = Path(__file__).resolve().parent
projectdir = scriptdir.parent

for path in projectdir.rglob('__pycache__'):
    shutil.rmtree(str(path))

for path in Path(projectdir, 'tests', 'coverage').iterdir():
    if path.name != '.gitignore':
        if path.is_file():
            path.unlink()
        else:
            shutil.rmtree(str(path))
