"""Run pep8 and pylint on all files."""

from pathlib import Path
import subprocess

import pep8

scriptdir = Path(__file__).resolve().parent
projectdir = scriptdir.parent

print('=' * 70 + '\nPEP 8\n' + '-' * 70)
pep8style = pep8.StyleGuide(ignore=['E402'])
pep8style.check_files(str(path) for path in projectdir.rglob('*.py'))

print('=' * 70 + '\nPYLINT\n' + '-' * 70)
args = ['/usr/bin/env', 'python3', '-m', 'pylint',
        '--rcfile={}/pylintrc'.format(scriptdir)]
args.extend(str(path) for path in projectdir.rglob('*.py'))
subprocess.run(args)
