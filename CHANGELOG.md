# Changelog #
## v0.2 (2016-09-23) ##
* Support for Python 2
* Improvement of unit tests
* Improvement of development scripts

## v0.1 (2016-04-27) ##
* Initial version
* Capacity and health checks
* Perf strings
