"""Nagios/NRPE plugin to monitor ZFS pools."""

from __future__ import division

import argparse
import sys
import subprocess

VERSION = '0.2'

STATES = {
    'OK': 0,
    'WARNING': 1,
    'CRITICAL': 2,
    'UNKNOWN': 3
}

ZPOOL_COMMAND = '/sbin/zpool'


def human_readable_bytes(number):
    """Convert a number of bytes to a human readable string."""
    PREFIXES = ['Ki', 'Mi', 'Gi', 'Ti', 'Pi']
    prefix = ''

    for p in PREFIXES:
        if number < 1024:
            break
        else:
            prefix = p
            number /= 1024

    numberstr = '{:.2f}'.format(float(number)).rstrip('0').rstrip('.')
    return '{} {}B'.format(numberstr, prefix)


class ZPoolQueryError(Exception):
    """
    Exception which is raised if information about a zpool could not be
    retrieved.
    """
    pass


def query_zpool(prop, poolname, parseable=False):
    """
    Retrieve a property from a pool. Optionally, parseable output can be
    requested.
    """
    command = ZPOOL_COMMAND.split()
    command.extend(['list', '-H'])
    if parseable:
        command.append('-p')
    command.extend(['-o', prop, poolname])

    try:
        output = subprocess.check_output(command).decode('ascii').rstrip()
    except (OSError, subprocess.CalledProcessError):
        raise ZPoolQueryError('zpool command could not be executed!')
    else:
        return output


def check_health(poolname):
    """
    Checks the health of a pool. Returns a status and an according message.
    """
    try:
        health = query_zpool('health', poolname)
    except ZPoolQueryError as e:
        return 'UNKNOWN', str(e)
    else:
        message = 'status: {}'.format(health)

        if health == 'ONLINE':
            return 'OK', message
        else:
            return 'CRITICAL', message


def check_capacity(poolname):
    """
    Checks the fill level of a pool. Returns a status and an according message,
    including a perfstring.
    """
    WARNING_LEVEL = 80
    CRITICAL_LEVEL = 90

    try:
        fill_level = int(query_zpool('capacity', poolname, parseable=True))
        free_space = int(query_zpool('free', poolname, parseable=True))
    except ZPoolQueryError as e:
        return 'UNKNOWN', str(e)
    except ValueError:
        return 'UNKNOWN', 'zpool command output could not be parsed!'
    else:
        message = 'fill level: {}% ({} free space)'.format(
            fill_level, human_readable_bytes(free_space)
        )
        perfstring = ' | fill_level={}%;{};{} free_space={}B'.format(
            fill_level, WARNING_LEVEL, CRITICAL_LEVEL, free_space
        )

        if 0 <= fill_level < WARNING_LEVEL:
            return 'OK', message + perfstring
        elif WARNING_LEVEL <= fill_level < CRITICAL_LEVEL:
            return 'WARNING', message + perfstring
        elif CRITICAL_LEVEL <= fill_level <= 100:
            return 'CRITICAL', message + perfstring
        else:
            return 'UNKNOWN', 'zpool command output could not be parsed!'


def emit(check, state, message):
    """
    Prints a message and state of a check. The exit code is set according to
    the state.
    """
    print('ZPOOL {} {} - {}'.format(check, state, message))
    sys.exit(STATES[state])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='NRPE plugin to check a ZFS pool.'
    )
    parser.add_argument(
        '-v', '--version', action='store_true', help='show version'
    )
    subparsers = parser.add_subparsers(
        dest='prop', description='the property to check'
    )

    parser_health = subparsers.add_parser(
        'health', help='check the health of a zpool'
    )
    parser_health.add_argument('poolname', help='name of the zpool')

    parser_capacity = subparsers.add_parser(
        'capacity', help='check the capacity of a zpool'
    )
    parser_capacity.add_argument('poolname', help='name of the zpool')

    try:
        args = parser.parse_args()
    except SystemExit:
        emit('STATUS', 'UNKNOWN', 'invalid script call!')
    else:
        if args.version is True:
            print(VERSION)
            emit('STATUS', 'UNKNOWN', 'invalid script call!')
        elif args.prop == 'health':
            emit('HEALTH', *check_health(args.poolname))
        elif args.prop == 'capacity':
            emit('CAPACITY', *check_capacity(args.poolname))
        else:
            emit('STATUS', 'UNKNOWN', 'invalid script call!')
